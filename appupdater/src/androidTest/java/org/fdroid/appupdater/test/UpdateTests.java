/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.appupdater.test;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.support.test.runner.AndroidJUnit4;
import org.fdroid.appupdater.UpdateRequest;
import org.fdroid.apputil.parser.PackageSpec;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;

@RunWith(AndroidJUnit4.class)
public class UpdateTests {
    private static final String APP = "org.witness.informacam.app";
    private static final String APP_NO_X86 = "info.guardianproject.orfox";

    private CountDownLatch responseLatch;
    private Throwable testError = null;
    private Map<String, PackageSpec> testResults = null;
    private Context mockContext;
    private PackageManager mockPackageManager;

    @Before
    public void setUp() {
        mockContext = Mockito.mock(Context.class);
        mockPackageManager = Mockito.mock(PackageManager.class);
        responseLatch = new CountDownLatch(1);
    }

    @Test
    public void useMocks() throws Exception {
        initMocks(APP, "d70ac6a02b53ebdd1354ea7af7b9ceee",
                202);

        assertEquals(APP, mockContext.getPackageName());
        assertNotNull(mockContext.getPackageManager());

        PackageInfo info = mockPackageManager.getPackageInfo(APP, PackageManager.GET_SIGNATURES);

        assertNotNull(info);
        assertEquals(202, info.versionCode);
        assertEquals(1, info.signatures.length);
        assertEquals("d70ac6a02b53ebdd1354ea7af7b9ceee",
                info.signatures[0].toCharsString());
    }

    @Test
    public void success() throws Throwable {
        initMocks(APP, "d70ac6a02b53ebdd1354ea7af7b9ceee", 202);

        UpdateRequest request = new UpdateRequest.Builder(mockContext)
                .fromCatalog(Uri.parse(BuildConfig.TEST_URL_GP))
                .asJson()
                .build();

        request.check(new UpdateCallback());
        responseLatch.await(10, TimeUnit.SECONDS);

        if (testError != null) {
            throw testError;
        }

        assertEquals(1, testResults.size());

        PackageSpec info = testResults.get(APP);

        assertNotNull(info);
        assertEquals(206, info.versioncode());
    }

    @Test
    public void noApp() throws Throwable {
        initMocks(APP, "d70ac6a02b53ebdd1354ea7af7b9ceee", 202);

        UpdateRequest request = new UpdateRequest.Builder(mockContext)
                .fromCatalog(Uri.parse(BuildConfig.TEST_URL_GP))
                .asJson()
                .forPackages("com.commonsware.this.so.does.not.exist")
                .build();

        request.check(new UpdateCallback());
        responseLatch.await(10, TimeUnit.SECONDS);

        if (testError != null) {
            throw testError;
        }

        assertEquals(0, testResults.size());
    }

    @Test
    public void nothingNewer() throws Throwable {
        initMocks(APP, "d70ac6a02b53ebdd1354ea7af7b9ceee", 206);

        UpdateRequest request = new UpdateRequest.Builder(mockContext)
                .fromCatalog(Uri.parse(BuildConfig.TEST_URL_GP))
                .asJson()
                .build();

        request.check(new UpdateCallback());
        responseLatch.await(10, TimeUnit.SECONDS);

        if (testError != null) {
            throw testError;
        }

        assertEquals(0, testResults.size());
    }

    @Test
    public void sigMismatch() throws Throwable {
        initMocks(APP, "a0eeebb161f946e3516945fae8a92a3e", 202);

        UpdateRequest request = new UpdateRequest.Builder(mockContext)
                .fromCatalog(Uri.parse(BuildConfig.TEST_URL_GP))
                .asJson()
                .build();

        request.check(new UpdateCallback());
        responseLatch.await(10, TimeUnit.SECONDS);

        if (testError != null) {
            throw testError;
        }

        assertEquals(0, testResults.size());
    }

    @Test
    public void arch() throws Throwable {
        initMocks(APP_NO_X86, "d70ac6a02b53ebdd1354ea7af7b9ceee", 3);

        UpdateRequest request = new UpdateRequest.Builder(mockContext)
                .fromCatalog(Uri.parse(BuildConfig.TEST_URL_GP))
                .asJson()
                .build();

        request.check(new UpdateCallback());
        responseLatch.await(10, TimeUnit.SECONDS);

        if (testError != null) {
            throw testError;
        }

        Set<String> abis = UpdateRequest.getSupportedABIs();

        if (abis.contains("armeabi-v7a")) {
            assertEquals(1, testResults.size());
        } else {
            assertEquals(0, testResults.size());
        }
    }

    private void initMocks(final String pkgName, final String sigHash,
                           final int versionCode)
            throws PackageManager.NameNotFoundException {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return (pkgName);
            }
        }).when(mockContext).getPackageName();

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return (mockPackageManager);
            }
        }).when(mockContext).getPackageManager();

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                PackageInfo info = new PackageInfo();
                Signature sig = new Signature(sigHash);

                info.signatures = new Signature[]{sig};
                info.versionCode = versionCode;

                return (info);
            }
        }).when(mockPackageManager).getPackageInfo(eq(pkgName), eq(PackageManager.GET_SIGNATURES));
    }

    private class UpdateCallback implements UpdateRequest.Callback {
        @Override
        public void onResult(Map<String, PackageSpec> results) {
            testResults = results;
            responseLatch.countDown();
        }

        @Override
        public void onError(Throwable t) {
            testError = t;
            responseLatch.countDown();
        }
    }
}
