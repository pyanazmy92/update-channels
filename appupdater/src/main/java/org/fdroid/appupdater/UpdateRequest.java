/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.appupdater;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import org.fdroid.apputil.common.HURLDownloadStrategy;
import org.fdroid.apputil.common.StreamDownloadStrategy;
import org.fdroid.apputil.parser.Application;
import org.fdroid.apputil.parser.Catalog;
import org.fdroid.apputil.parser.CatalogParser;
import org.fdroid.apputil.parser.JsonCatalogParser;
import org.fdroid.apputil.parser.PackageSpec;
import org.fdroid.apputil.parser.XppCatalogParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Checks an F-Droid catalog for app(s) and identifies if there are any valid
 * updates available, based on version code and signature match.
 *
 * Create an instance of this class using UpdateRequest.Builder, then call
 * check() to perform the check.
 */
public class UpdateRequest {
  /**
   * Interface for results to be delivered asynchronously to the caller of check()
   */
  public interface Callback {
    /**
     * Called with the results of the check. Each entry in the Map represents one package,
     * based on its application ID (package name), with the PackageSpec showing
     * the details of that package. If you requested a package, and it does
     * not appear in the results, it is up to date.
     *
     * @param results the map of packages to available updates
     */
    void onResult(Map<String, PackageSpec> results);

    /**
     * Called in case there is a problem implementing the check.
     *
     * @param t details of what went wrong
     */
    void onError(Throwable t);
  }

  private final Context ctxt;
  private final Uri catalog;
  private final CatalogParser parser;
  private final StreamDownloadStrategy strategy;
  private final String[] packages;

  private UpdateRequest(Context ctxt, Uri catalog, CatalogParser parser,
                        StreamDownloadStrategy strategy, String[] packages) {

    this.ctxt=ctxt;
    this.catalog=catalog;
    this.parser=parser;
    this.strategy=strategy;
    this.packages=packages;
  }

  /**
   * Check to see whether the requested packages have updates or not.
   *
   * @param cb the callback for getting results asynchronously
   * @throws FileNotFoundException thrown if you provide a Uri that is not
   * http(s) and a ContentResolver cannot find the file that you cited
   */
  public void check(@NonNull final Callback cb) throws FileNotFoundException {
    if (strategy==null) {
      process(ctxt.getContentResolver().openInputStream(catalog), cb);
    }
    else {
      strategy.download(catalog.toString(), new StreamDownloadStrategy.Callback() {
        @Override
        public void onSuccess(@NonNull String url, @NonNull InputStream content)
          throws Exception {
          process(content, cb);
        }

        @Override
        public void onError(@NonNull String url, @NonNull Throwable t) {
          cb.onError(t);
        }
      });
    }
  }

  private void process(InputStream in, final Callback cb) {
    try {
      Catalog catalog=parser.parse(in, packages);
      Map<String, PackageSpec> results=new HashMap<>();
      PackageManager pm=ctxt.getPackageManager();

      for (String pkg : packages) {
        Application app=catalog.apps().get(pkg);
        PackageSpec bestMatch=null;
        PackageInfo info=pm.getPackageInfo(pkg, PackageManager.GET_SIGNATURES);

        if (info!=null && info.signatures.length==1) {
          String sigHash=info.signatures[0].toCharsString();

          for (PackageSpec spec : app.packages()) {
            if (isMatch(spec, info.versionCode, sigHash)) {
              if (bestMatch==null ||
                spec.versioncode()>bestMatch.versioncode()) {
                bestMatch=spec;
              }
            }
          }

          if (bestMatch!=null) {
            results.put(pkg, bestMatch);
          }
        }
      }

      cb.onResult(results);
    }
    catch (Throwable t) {
      cb.onError(t);
    }
  }

  private boolean isMatch(PackageSpec spec, int versionCode, String sigHash) {
    if (versionCode<spec.versioncode()) {
      if (sigHash.equals(spec.sig())) {
        if (abiMatch(spec.nativecode())) {
          return(true);
        }
      }
    }

    return(false);
  }

  private boolean abiMatch(List<String> nativecode) {
    Set<String> device=getSupportedABIs();

    device.retainAll(nativecode);

    return(device.size()>0);
  }

  public static Set<String> getSupportedABIs() {
    Set<String> result=new HashSet<>();

    result.add(Build.CPU_ABI);
    result.add(Build.CPU_ABI2);

    if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {
      Collections.addAll(result, Build.SUPPORTED_ABIS);
    }

    return(result);
  }

  /**
   * Use this to create instances of UpdateRequest. As with most builder-style
   * APIs, call the builder methods (each of which return the Builder for
   * chaining), then call build() to create the UpdateRequest object.
   */
  public static class Builder {
    private static final String SCHEME_HTTP="http";
    private static final String SCHEME_HTTPS="https";
    final Context ctxt;
    private Uri catalog;
    private StreamDownloadStrategy strategy;
    private CatalogParser parser;
    private String[] packages;

    /**
     * Standard constructor
     *
     * @param ctxt a Context to use where needed (e.g., for local file lookups)
     */
    public Builder(@NonNull Context ctxt) {
      this.ctxt=ctxt;
    }

    /**
     * Identify the source of the catalog to check for updates. If you supply
     * an http(s) Uri, it will be downloaded; if you supply anything else, the
     * Uri will be passed to a ContentResolver to try to load in the catalog.
     *
     * @param catalog a Uri pointing to the catalog that you want to check
     * @return the builder, for further configuration
     */
    public Builder fromCatalog(@NonNull Uri catalog) {
      this.catalog=catalog;

      return(this);
    }

    /**
     * Supply a custom CatalogParser to parse the F-Droid catalog. Alternatively,
     * just call asJson() or asXml() to use stock parsers for those formats.
     *
     * @param parser a CatalogParser for, um, parsing the catalog
     * @return the builder, for further configuration
     */
    public Builder parseUsing(@NonNull CatalogParser parser) {
      this.parser=parser;

      return(this);
    }

    /**
     * Indicates that the catalog is an F-Droid JSON catalog and should be
     * parsed as such.
     *
     * @return the builder, for further configuration
     */
    public Builder asJson() {
      this.parser=new JsonCatalogParser();

      return(this);
    }

    /**
     * Indicates that the catalog is an F-Droid XML catalog and should be
     * parsed as such.
     *
     * @return the builder, for further configuration
     */
    public Builder asXml() throws XmlPullParserException {
      this.parser=new XppCatalogParser();

      return(this);
    }

    /**
     * If you supplied an http(s) Uri to fromCatalog(), here you supply
     * the means of downloading it. If you skip this call, a default
     * download mechanism will be used.
     *
     * However, if at all possible, it is strongly recommended that you use
     * the OkHttp3DownloadStrategy, configured with a suitable OkHttpClient,
     * for handling things like network security configuration, NetCipher
     * integration, caching, and so forth.
     *
     * @param strategy the way to download the catalog
     * @return the builder, for further configuration
     */
    public Builder downloadUsing(@NonNull StreamDownloadStrategy strategy) {
      this.strategy=strategy;

      return(this);
    }

    /**
     * Identifies the apps for which you want to check for updates. If you
     * fail to call this, we will check the app identified by getPackageName()
     * called on the Context that you supplied to the constructor.
     *
     * @param packages the application IDs (package names) to check for
     * @return the builder, for further configuration
     */
    public Builder forPackages(String... packages) {
      this.packages=packages;

      return(this);
    }

    /**
     * Builds the UpdateRequest to your specifications.
     *
     * @return a configured UpdateRequest
     */
    public UpdateRequest build() {
      if (catalog==null) {
        throw new IllegalStateException("We need a repository Uri");
      }

      if ((SCHEME_HTTP.equals(catalog.getScheme()) ||
        SCHEME_HTTPS.equals(catalog.getScheme()))) {
        if (strategy==null) {
          strategy=new HURLDownloadStrategy();
        }
      }

      if (parser==null) {
        throw new IllegalStateException("We need a CatalogParser for the catalog type");
      }

      if (packages==null ||
        (packages.length==1 && TextUtils.isEmpty(packages[0]))) {
        packages=new String[] { ctxt.getPackageName() };
      }

      return(new UpdateRequest(ctxt, catalog, parser, strategy, packages));
    }
  }
}
