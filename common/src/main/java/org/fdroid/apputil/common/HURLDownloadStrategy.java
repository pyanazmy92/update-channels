/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.common;

import android.support.annotation.NonNull;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * FileDownloadStrategy using HttpURLConnection. Use this if you do not want
 * OkHttp3 in your project for some reason and therefore cannot use
 * OkHttp3DownloadStrategy.
 */
public class HURLDownloadStrategy
  implements FileDownloadStrategy, StreamDownloadStrategy {
  /**
   * @{inheritDoc}
   */
  @Override
  public void download(final @NonNull String url, final @NonNull File dest,
                       final @NonNull FileDownloadStrategy.Callback cb) {
    new DownloadThread(url, new StreamDownloadStrategy.Callback() {
      @Override
      public void onSuccess(@NonNull String url, @NonNull InputStream content)
        throws Exception {
        BufferedOutputStream out=new BufferedOutputStream(new FileOutputStream(dest));

        try {
          byte[] buffer=new byte[65536];
          int len;

          while ((len=content.read(buffer))>=0) {
            out.write(buffer, 0, len);
          }

          out.flush();
        }
        finally {
          out.close();
          cb.onSuccess(url);
        }
      }

      @Override
      public void onError(@NonNull String url, @NonNull Throwable t) {
        cb.onError(url, t);
      }
    }).start();
  }

  /**
   * @{inheritDoc}
   */
  @Override
  public void download(@NonNull String url,
                       @NonNull StreamDownloadStrategy.Callback cb) {
    new DownloadThread(url, cb).start();
  }

  @SuppressWarnings("TryFinallyCanBeTryWithResources")
  private static class DownloadThread extends Thread {
    final String url;
    final StreamDownloadStrategy.Callback cb;

    private DownloadThread(String url, StreamDownloadStrategy.Callback cb) {
      this.url=url;
      this.cb=cb;
    }

    @Override
    public void run() {
      try {
        HttpURLConnection c=(HttpURLConnection)new URL(url).openConnection();

        if (c.getResponseCode()==200) {
          try {
            cb.onSuccess(url, c.getInputStream());
            c.disconnect();
          }
          catch (Throwable t) {
            cb.onError(url, t);
          }
        }
        else {
          cb.onError(url, new FileNotFoundException(url+" not found"));
        }
      }
      catch (Throwable t) {
        cb.onError(url, t);
      }
    }
  }
}
