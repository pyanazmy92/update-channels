/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PackageSpec {
    private String version;
    private int versioncode;
    private String apkname;
    private int size;
    private String sdkver;
    private String targetSdkVersion;
    private long added;
    private String sig;
    private List<String> permissions = new ArrayList<>();
    private List<String> nativecode = new ArrayList<>();
    private String hashType;
    private String hashValue;

    public String hashType() {
        return (hashType);
    }

    void hashType(String type) {
        this.hashType = type;
    }

    public String hashValue() {
        return (hashValue);
    }

    void hashValue(String value) {
        this.hashValue = value;
    }

    public String version() {
        return (version);
    }

    void version(String version) {
        this.version = version;
    }

    public int versioncode() {
        return (versioncode);
    }

    void versioncode(int versioncode) {
        this.versioncode = versioncode;
    }

    public String apkname() {
        return (apkname);
    }

    void apkname(String apkname) {
        this.apkname = apkname;
    }

    public int size() {
        return (size);
    }

    void size(int size) {
        this.size = size;
    }

    public String sdkver() {
        return (sdkver);
    }

    void sdkver(String sdkver) {
        this.sdkver = sdkver;
    }

    public String targetSdkVersion() {
        return (targetSdkVersion);
    }

    void targetSdkVersion(String targetSdkVersion) {
        this.targetSdkVersion = targetSdkVersion;
    }

    public long added() {
        return (added);
    }

    void added(long added) {
        this.added = added;
    }

    public String sig() {
        return (sig);
    }

    void sig(String sig) {
        this.sig = sig;
    }

    public List<String> permissions() {
        return (permissions);
    }

    void addPermissions(String... permissions) {
        Collections.addAll(this.permissions, permissions);
    }

    public List<String> nativecode() {
        return (nativecode);
    }

    void addNativecode(String... nativecode) {
        Collections.addAll(this.nativecode, nativecode);
    }
}
