/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.parser;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import java.io.InputStream;

/**
 * Base class for all catalog parsers.
 */
@WorkerThread
public abstract class CatalogParser {
    /**
     * Parses a catalog from the supplied stream.
     *
     * @param in     the catalog, from a file, HTTP(S) stream, asset, etc.
     * @param filter means of identifying the Applications of interest, or null
     *               to include all Applications in the output
     * @return the parsed Catalog, containing the identified Applications
     * @throws Exception because things go wrong
     */
    abstract public Catalog parse(@NonNull InputStream in, ApplicationFilter filter)
            throws Exception;

    /**
     * Parses a catalog from the supplied stream.
     *
     * @param in the catalog, from a file, HTTP(S) stream, asset, etc.
     * @return the parsed Catalog, including all possible Applications
     * @throws Exception because boom happens
     */
    public Catalog parse(InputStream in) throws Exception {
        return (parse(in, (ApplicationFilter) null));
    }

    /**
     * Parses a catalog from the supplied stream, constraining the included
     * Applications to those identified by the supplied identifiers
     *
     * @param in  the catalog, from a file, HTTP(S) stream, asset, etc.
     * @param ids one or more application IDs (package names) to find and include
     *            in the parsed catalog
     * @return the parsed Catalog, containing the identified Applications
     * @throws Exception because oopsie!
     */
    public Catalog parse(InputStream in, String... ids) throws Exception {
        return (parse(in, new SimpleApplicationFilter(ids)));
    }

    /**
     * Used to constrain the number of Applications that we keep in memory to
     * the subset identified by this filter.
     */
    interface ApplicationFilter {
        /**
         * @param app an Application that was parsed from the catalog
         * @return true if you want to have this included in the results, false
         * otherwise
         */
        boolean isAcceptable(@NonNull Application app);
    }
}
