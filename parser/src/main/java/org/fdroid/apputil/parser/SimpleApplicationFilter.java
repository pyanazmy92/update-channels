/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.parser;

import android.support.annotation.NonNull;
import org.fdroid.apputil.parser.CatalogParser.ApplicationFilter;

import java.util.Arrays;

/**
 * A rudimentary filter, based on an array of identifiers
 */
class SimpleApplicationFilter implements ApplicationFilter {
    private final String[] sorted;

    /**
     * Save the identifiers in sorted form, for use with binarySearch()
     *
     * @param ids the identifiers to filter upon
     */
    SimpleApplicationFilter(String[] ids) {
        Arrays.sort(ids);

        sorted = ids;
    }

    /**
     * @{inheritDoc}
     */
    @Override
    public boolean isAcceptable(@NonNull Application app) {
        return (Arrays.binarySearch(sorted, app.id()) >= 0);
    }
}
