package org.fdroid.apputil.parser.test;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.text.TextUtils;
import org.fdroid.apputil.parser.Application;
import org.fdroid.apputil.parser.JsonCatalogParser;
import org.fdroid.apputil.parser.PackageSpec;
import org.fdroid.apputil.parser.XppCatalogParser;
import org.fdroid.apputil.parser.Catalog;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.io.InputStream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class ParserTest {
  @Test
  public void testXml() throws Exception {
    InputStream in=
      InstrumentationRegistry.getContext().getAssets().open("guardianproject-repo.jar");
    Catalog catalog=new XppCatalogParser().parse(in);

    assertNotNull(catalog);
    checkCatalogBasics(catalog, true);
    assertEquals(10, catalog.apps().size());
    checkApp(catalog.apps().get("org.witness.informacam.app"), true);
  }

  @Test
  public void testXmlFilter() throws Exception {
    InputStream in=InstrumentationRegistry.getContext().getAssets().open("guardianproject-repo.jar");
    Catalog catalog=new XppCatalogParser().parse(in,
      "org.witness.informacam.app");

    assertNotNull(catalog);
    checkCatalogBasics(catalog, true);
    assertEquals(1, catalog.apps().size());
    checkApp(catalog.apps().get("org.witness.informacam.app"), true);
  }

  @Test
  public void testJson() throws Exception {
    InputStream in=InstrumentationRegistry.getContext().getAssets().open("index-v1.jar");
    Catalog catalog=new JsonCatalogParser().parse(in);

    assertNotNull(catalog);
    checkCatalogBasics(catalog, false);
    assertEquals(10, catalog.apps().size());
    checkApp(catalog.apps().get("org.witness.informacam.app"), false);
  }

  @Test
  public void testJsonFilter() throws Exception {
    InputStream in=InstrumentationRegistry.getContext().getAssets().open("index-v1.jar");
    Catalog catalog=
      new JsonCatalogParser().parse(in, "org.witness.informacam.app");

    assertNotNull(catalog);
    checkCatalogBasics(catalog, false);
    assertEquals(1, catalog.apps().size());
    checkApp(catalog.apps().get("org.witness.informacam.app"), false);
  }

  private void checkCatalogBasics(Catalog catalog, boolean isXML) {
    assertNotNull(catalog.repo());
    assertEquals("guardianproject.png", catalog.repo().icon());
    assertEquals("Guardian Project Official Releases", catalog.repo().name());
    assertEquals(18, catalog.repo().version());
    assertEquals("The official app repository of The Guardian Project. Applications in this repository are official binaries build by the original application developers and signed by the same key as the APKs that are released in the Google Play store.", catalog.repo().description().trim());
    assertEquals(5, catalog.repo().mirrors().size());
    assertTrue(catalog.repo().mirrors().contains("http://bdf2wcxujkg6qqff.onion/fdroid/repo"));
    assertTrue(catalog.repo().mirrors().contains("https://gitlab.com/guardianproject/fdroid-repo/raw/master/fdroid/"));

    if (isXML) {
      assertEquals(1492223297, catalog.repo().timestamp());
      assertEquals("https://guardianproject.info/fdroid/repo", catalog.repo().url());
      assertEquals(60, catalog.repo().maxage());
      assertEquals(
        "308205d8308203c0020900a397b4da7ecda034300d06092a864886f70d01010505003081ad310b30090603550406130255533111300f06035504080c084e657720596f726b3111300f06035504070c084e657720596f726b31143012060355040b0c0b4644726f6964205265706f31193017060355040a0c10477561726469616e2050726f6a656374311d301b06035504030c14677561726469616e70726f6a6563742e696e666f3128302606092a864886f70d0109011619726f6f7440677561726469616e70726f6a6563742e696e666f301e170d3134303632363139333931385a170d3431313131303139333931385a3081ad310b30090603550406130255533111300f06035504080c084e657720596f726b3111300f06035504070c084e657720596f726b31143012060355040b0c0b4644726f6964205265706f31193017060355040a0c10477561726469616e2050726f6a656374311d301b06035504030c14677561726469616e70726f6a6563742e696e666f3128302606092a864886f70d0109011619726f6f7440677561726469616e70726f6a6563742e696e666f30820222300d06092a864886f70d01010105000382020f003082020a0282020100b3cd79121b9b883843be3c4482e320809106b0a23755f1dd3c7f46f7d315d7bb2e943486d61fc7c811b9294dcc6b5baac4340f8db2b0d5e14749e7f35e1fc211fdbc1071b38b4753db201c314811bef885bd8921ad86facd6cc3b8f74d30a0b6e2e6e576f906e9581ef23d9c03e926e06d1f033f28bd1e21cfa6a0e3ff5c9d8246cf108d82b488b9fdd55d7de7ebb6a7f64b19e0d6b2ab1380a6f9d42361770d1956701a7f80e2de568acd0bb4527324b1e0973e89595d91c8cc102d9248525ae092e2c9b69f7414f724195b81427f28b1d3d09a51acfe354387915fd9521e8c890c125fc41a12bf34d2a1b304067ab7251e0e9ef41833ce109e76963b0b256395b16b886bca21b831f1408f836146019e7908829e716e72b81006610a2af08301de5d067c9e114a1e5759db8a6be6a3cc2806bcfe6fafd41b5bc9ddddb3dc33d6f605b1ca7d8a9e0ecdd6390d38906649e68a90a717bea80fa220170eea0c86fc78a7e10dac7b74b8e62045a3ecca54e035281fdc9fe5920a855fde3c0be522e3aef0c087524f13d973dff3768158b01a5800a060c06b451ec98d627dd052eda804d0556f60dbc490d94e6e9dea62ffcafb5beffbd9fc38fb2f0d7050004fe56b4dda0a27bc47554e1e0a7d764e17622e71f83a475db286bc7862deee1327e2028955d978272ea76bf0b88e70a18621aba59ff0c5993ef5f0e5d6b6b98e68b70203010001300d06092a864886f70d0101050500038202010079c79c8ef408a20d243d8bd8249fb9a48350dc19663b5e0fce67a8dbcb7de296c5ae7bbf72e98a2020fb78f2db29b54b0e24b181aa1c1d333cc0303685d6120b03216a913f96b96eb838f9bff125306ae3120af838c9fc07ebb5100125436bd24ec6d994d0bff5d065221871f8410daf536766757239bf594e61c5432c9817281b985263bada8381292e543a49814061ae11c92a316e7dc100327b59e3da90302c5ada68c6a50201bda1fcce800b53f381059665dbabeeb0b50eb22b2d7d2d9b0aa7488ca70e67ac6c518adb8e78454a466501e89d81a45bf1ebc350896f2c3ae4b6679ecfbf9d32960d4f5b493125c7876ef36158562371193f600bc511000a67bdb7c664d018f99d9e589868d103d7e0994f166b2ba18ff7e67d8c4da749e44dfae1d930ae5397083a51675c409049dfb626a96246c0015ca696e94ebb767a20147834bf78b07fece3f0872b057c1c519ff882501995237d8206b0b3832f78753ebd8dcbd1d3d9f5ba733538113af6b407d960ec4353c50eb38ab29888238da843cd404ed8f4952f59e4bbc0035fc77a54846a9d419179c46af1b4a3b7fc98e4d312aaa29b9b7d79e739703dc0fa41c7280d5587709277ffa11c3620f5fba985b82c238ba19b17ebd027af9424be0941719919f620dd3bb3c3f11638363708aa11f858e153cf3a69bce69978b90e4a273836100aa1e617ba455cd00426847f",
        catalog.repo().pubkey());
      assertNotNull(catalog.install());
      assertEquals("org.torproject.android", catalog.install().packageName());
    }
    else {
      assertEquals(1492618828602L, catalog.repo().timestamp());
    }
  }

  private void checkApp(Application app, boolean isXML) {
    assertEquals(1386738000000L, app.added());
    assertEquals(1446526800000L, app.lastUpdated());
    assertEquals("CameraV", app.name());
    assertEquals("An InformaCam app to generate verifiable media",
      app.summary());
    assertEquals("org.witness.informacam.app.206.png", app.icon());
    assertEquals("<p>An InformaCam app to generate verifiable media.</p>",
      app.desc());
    assertEquals("GPLv3", app.license());
    assertEquals(2, app.categories().size());
    assertTrue(app.categories().contains("Development"));
    assertTrue(app.categories().contains("GuardianProject"));
    assertEquals("https://guardianproject.info/apps/camerav/", app.web());
    assertEquals("https://github.com/guardianproject/CameraV",
      app.source());
    assertEquals("https://dev.guardianproject.info/projects/informacam/issues",
      app.tracker());
    assertEquals("1Fi5xUHiAPRKxHvyUGVFGt9extBe8Srdbk", app.bitcoin());
    assertTrue(TextUtils.isEmpty(app.marketversion()));
    assertEquals("9999999", app.marketvercode());

    assertEquals(3, app.packages().size());

    PackageSpec pkg=null;

    for (PackageSpec candidate : app.packages()) {
      if ("0.2.6".equals(candidate.version())) {
        pkg=candidate;
        break;
      }
    }

    assertNotNull(pkg);
    assertEquals(206, pkg.versioncode());
    assertEquals("CameraVApp-release-0.2.6.apk", pkg.apkname());
    assertEquals("sha256", pkg.hashType());
    assertEquals("508f453e26c8c83dba858b53b21d909d549fe5646d01eb198c96c22d8e521e7c",
      pkg.hashValue());
    assertEquals(24123646, pkg.size());
    assertEquals("16", pkg.sdkver());
    assertEquals("21", pkg.targetSdkVersion());
    assertEquals(1446526800000L, pkg.added());
    assertEquals("d70ac6a02b53ebdd1354ea7af7b9ceee", pkg.sig());
    assertEquals(19, pkg.permissions().size());
    assertEquals(3, pkg.nativecode().size());
    assertTrue(pkg.nativecode().contains("armeabi"));
    assertTrue(pkg.nativecode().contains("armeabi-v7a"));
    assertTrue(pkg.nativecode().contains("x86"));

    if (isXML) {
      assertEquals("Development", app.category());
      assertTrue(pkg.permissions().contains("ACCESS_COARSE_LOCATION"));
      assertTrue(pkg.permissions().contains("ACCESS_WIFI_STATE"));
      assertTrue(pkg.permissions().contains("WRITE_EXTERNAL_STORAGE"));
    }
    else {
      assertTrue(pkg.permissions().contains("android.permission.ACCESS_COARSE_LOCATION"));
      assertTrue(pkg.permissions().contains("android.permission.ACCESS_WIFI_STATE"));
      assertTrue(pkg.permissions().contains("android.permission.WRITE_EXTERNAL_STORAGE"));
    }
  }
}

